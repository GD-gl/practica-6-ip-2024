from math import sqrt, pi, ceil

# 1.1
def imprimir_hola_mundo():
    print("¡Hola mundo!")

# 1.2
def imprimir_un_verso():
    choclo = "Facts on the move again\nTruths on the hoof they say\nOh, can't you see\nIt's lost in the everyday\nIt slid so far away\nFrom you and me"
    print(choclo)

# 1.3
def raizDe2() -> float:
    return round(sqrt(2.0), 4)

# 1.4
def factorial_2() -> int:
    res = 1
    hasta = 2
    for i in range(1, hasta + 1):
        res = res * i
    return res

# 1.5
def perimetro() -> float:
    return 2 * pi

# 2.1
def imprimir_saludo(nombre: str):
    print('Hola ' + nombre)

# 2.2
def raiz_cuadrada_de(numero: float):
    return sqrt(numero)

# 2.3
def fahrenheit_a_celsius(t: float) -> float:
    return ((t - 32) * 5) / 9;

# 2.4
def imprimir_dos_veces(estribillo: str):
    # print(estribillo * 2)
    for _ in range(1, 3):
        print(estribillo)

# 2.5, m != 0
def es_multiplo_de(n: int, m: int) -> bool:
    m_abs: int = abs(m)
    n_abs: int = abs(n)
    for k in range(0, n_abs + 1):
        if m_abs * k == n_abs:
            return True
    return False

# 2.6
def es_par(num: int) -> bool:
    return es_multiplo_de(num, 2)

# 2.7
def cantidad_de_pizzas(comensales: int, min_cantidad_de_porciones: int) -> int:
    return ceil((min_cantidad_de_porciones * comensales) / 8)

# 3.1
def alguno_es_0(num1: int, num2: int) -> bool:
    return num1 == 0 or num2 == 0

# 3.2
def ambos_son_0(num1: int, num2: int) -> bool:
    return num1 == 0 and num2 == 0

# 3.3
def es_nombre_largo(nombre: str) -> bool:
    return 3 <= len(nombre) and len(nombre) <= 8

# 3.4
def es_bisiesto(y: int):
    return es_multiplo_de(y, 400) or (es_multiplo_de(y, 4) and not es_multiplo_de(y, 100))

# 4 - funcional
def peso_pino(altura_metros: int):
    return (min(altura_metros, 3) * 100 * 3) + (max((altura_metros - 3, 0)) * 100 * 2)

def es_peso_util(peso: int) -> bool:
    return peso >= 400 and peso <= 1000

def sirve_pino(altura: int):
    return es_peso_util(peso_pino(altura))

# 5.1
"""
problema devolver_el_doble_si_es_par(n: Z): Z {
    requiere: {True}
    asegura: {res = n * 2 <=> abs(n) mod 2 == 0}
    asegura: {res = n <=> abs(n) mod 2 != 0}
}
"""

def devolver_el_doble_si_es_par(n: int) -> int:
    if es_multiplo_de(n, 2):
        return n * 2
    else:
        return n

# 5.2 - lo hago con doble if porque funciona, con if+else obviamente tamb
"""
problema devolver_valor_si_es_par_sino_el_que_sigue(n: Z): Z {
    requiere: {True}
    asegura: {res = n + 1 <=> abs(n) mod 2 == 0}
    asegura: {res = n <=> abs(n) mod 2 != 0}
}
"""

def devolver_valor_si_es_par_sino_el_que_sigue(n: int) -> int:
    if es_multiplo_de(n, 2):
        return n + 1
    if not es_multiplo_de(n, 2):
        return n

# 5.3 - que asco el nombre de la funcion
"""
problema devolver_el_doble_si_es_multiplo3_el_triple_si_es_multiplo9(n: Z): Z {
    requiere: {True}
    asegura: {res = n * 2 <=> abs(n) mod 3 == 0}
    asegura: {res = n * 3 <=> abs(n) mod 9 == 0}
    asegura: {res = n <=> abs(n) mod 2 != 0}
}
"""

# nunca se va a cumplir lo de si es multiplo de 9 asi que ni lo pongo
# total si es multiplo de 9 ya es multiplo de 3
def devolver_el_doble_si_es_multiplo3_el_triple_si_es_multiplo9(n: int) -> int:
    if es_multiplo_de(n, 3):
        return n * 2
    else:
        return n

# 5.4
"""
problema lindo_nombre(n: string): string {
    requiere: {True}
    asegura: {res = "Tu nombre tiene muchas letras!" <=> |n| >= 5}
    asegura: {res = "Tu nombre tiene menos de 5 caracteres" <=> |n| < 5}
}
"""

def lindo_nombre(n: str) -> str:
    if len(n) >= 5:
        return "Tu nombre tiene muchas letras!"
    else:
        return "Tu nombre tiene menos de 5 caracteres"

# 5.5
"""
problema elRango(n: Z): Z {
    requiere: {True}
    asegura: {imprime en pantalla "Menor a 5" <=> n < 5}
    asegura: {imprime en pantalla "Entre 10 y 20" <=> 10 <= n <= 20}
    asegura: {imprime en pantalla "Mayor a 20" <=> n > 20}
}
"""

def elRango(n: int) -> int:
    if n < 5:
        print("Menor a 5")
    if 10 <= n and n <= 20:
        print("Entre 10 y 20")
    if n > 20:
        print("Mayor a 20")

# 5.6
"""
problema vacaciones_o_laburo(edad: Z, es_hombre: bool) {
    requiere: {edad >= 0}
    asegura: {imprime "Andá de vacaciones" <=> (es_hombre && edad >= 65) || (!es_hombre && edad >= 60) || edad < 18}
    asegura: {imprime "Te toca trabajar" <=> (es_hombre && edad < 65 && edad >= 18) || (!es_hombre && edad < 60 && edad >= 18)}
}
"""
def vacaciones_o_laburo(edad: int, es_hombre: bool):
    if (es_hombre and edad >= 65) or (not es_hombre and edad >= 60) or edad < 18:
        print("Andá de vacaciones")
    else:
        print("Te toca trabajar")

# 6.1
def uno_a_diez():
    i = 1
    while i <= 10:
        print(i)
        i += 1

# 6.2
def pares_10_a_40():
    i = 10
    while i <= 40:
        print(i)
        i += 2

# 6.3
def eco_10():
    i = 1
    while i <= 10:
        print("eco")
        i += 1

# 6.4
def despegue(n: int):
    while n > 0:
        print(n)
        n -= 1
    print("Despegue")

# 6.5, llegada < partida
def viaje_tiempo(y_partida: int, y_llegada: int):
    while y_partida > y_llegada:
        y_partida -= 1
        print("Viajó un año al pasado, estamos en el año:", y_partida)

# 6.6
def viaje_tiempo_aristoteles(y_partida: int):
    while y_partida > 384:
        y_partida -= 20
        if y_partida < 384:
            break
        print("Viajó un año al pasado, estamos en el año:", y_partida)

# 7.1
def s_uno_a_diez():
    for i in range(1, 11):
        print(i)

# 7.2
def s_pares_10_a_40():
    for i in range(10, 41, 2):
        print(i)

# 7.3
def s_eco_10():
    for _ in range(0, 10):
        print("eco")

# 7.4
def s_despegue(n: int):
    for i in range(n, 0, -1):
        print(i)
    print("Despegue")

# 7.5, llegada < partida
def s_viaje_tiempo(y_partida: int, y_llegada: int):
    for y in range(y_partida, y_llegada - 1, -1):
        print("Viajó un año al pasado, estamos en el año:", y)

# 7.6
def s_viaje_tiempo_aristoteles(y_partida: int):
    for y in range(y_partida, 384 - 1, -20):
        print("Viajó un año al pasado, estamos en el año:", y)

# 8.1
"""
# Estado a
x = 5
# Estado b
# vale x == 5
y = 7
# Estado c
# vale x == 5 ^ y == 7
x = x + y
# Estado d
# vale x == x@b + y@c ^ y == 7
"""

# 8.2
"""
# Estado a
x = 5
# Estado b
# vale x == 5
y = 7
# Estado c
# vale x == 5 ^ y == 7
z = x + y
# Estado d
# vale z == x@b + y@c^ x == 5 ^ y == 7
y = z * 2
# Estado e
# vale z == x@b + y@c^ x == 5 ^ y == z@d * 2
"""

# 8.3
"""
# Estado a
x = 5
# Estado b
# vale x == 5
y = 7
# Estado c
# vale x == 5 ^ y == 7
x = "hora"
# Estado d
# vale x == "hora" ^ y == 7
y = x * 2
# Estado e
# vale x == "hora" ^ y = x@d * 2
"""

# 8.4
"""
# Estado a
x = False
# Estado b
# vale x == False
res = not x
# Estado c
# vale x == False ^ res = not x@b
"""

# 8.5
"""
# Estado a
x = False
# Estado b
# vale x == False
x = not x
# Estado c
# vale x == not x@b
"""

# 8.6
"""
# Estado a
x = True
# Estado b
# vale x == True
y = False
# Estado c
# vale x == True ^ y == False
res = x and y
# Estado d
# vale res = x@b and y@c ^ x == True ^ y == False
x = res and x
# Estado e
# vale res = x@b and y@c ^ x == res@d and x@b ^ y = False
"""

# 9.1 y 9.3:
"""
# Estado a
# Vale g == 0
x = ro(1) # x es 2
# Estado b
# Vale g == 1 ^ x == 2
x = ro(1) # x es 3
# Vale g == 2 ^ x == 3
x = ro(1) # x es 4
# Vale g == 3 ^ x == 4
"""

# 9.2 y 9.3:
"""
# Estado a
x = rt(1, 0) # x es 2
# Estado b
# vale x == 2
x = rt(1, 0) # x es 2
# Estado c
# vale x == 2
x = rt(1, 0) # x es 2
# Estado d
# vale x == 2
"""

# 9.4: (dudoso, seguramente se puede hacer mejor)
"""
problema rt(in x: Z, in g: Z): Z {
    requiere: {True}
    asegura: {res = x + g + 1}
}

problema ro(in x: Z): Z {
    requiere: {True}
    modifica: {g = g + 1}
    asegura: {res = x + g}
}
"""

# pruebo todo
print('--- 1 ---')
imprimir_hola_mundo()
imprimir_un_verso()
print(raizDe2())
print(factorial_2())
print(perimetro())

print('\n--- 2 ---')
imprimir_saludo("yo")
print(raiz_cuadrada_de(9))
print(fahrenheit_a_celsius(100))
imprimir_dos_veces("holaaa\naaaa")
print(es_multiplo_de(5, 3))
print(es_multiplo_de(15, 3))
print(es_multiplo_de(208, 3))
print(es_multiplo_de(-333, 9))
print(es_par(-333))
print(es_par(9827614))
print(cantidad_de_pizzas(10, 5))

print('\n--- 3 ---')
print(alguno_es_0(1, 0))
print(alguno_es_0(0, 1))
print(alguno_es_0(1, 1))
print(alguno_es_0(0, 0))
print(ambos_son_0(0, 0))
print(es_nombre_largo("si"))
print(es_nombre_largo("kjlasdkljskjl"))
print(es_nombre_largo("holaa"))
print(es_bisiesto(1904))
print(es_bisiesto(2000))

print('\n--- 4 ---')
print(peso_pino(2))
print(peso_pino(5))
print(es_peso_util(300))
print(es_peso_util(400))
print(es_peso_util(738))
print(es_peso_util(1000))
print(es_peso_util(1100))

print('\n--- 5 ---')
print(devolver_el_doble_si_es_par(7))
print(devolver_el_doble_si_es_par(8))
print(devolver_valor_si_es_par_sino_el_que_sigue(7))
print(devolver_valor_si_es_par_sino_el_que_sigue(8))
print(devolver_el_doble_si_es_multiplo3_el_triple_si_es_multiplo9(2))
print(devolver_el_doble_si_es_multiplo3_el_triple_si_es_multiplo9(6))
print(devolver_el_doble_si_es_multiplo3_el_triple_si_es_multiplo9(18))
print(lindo_nombre("abc"))
print(lindo_nombre("holis"))
elRango(3)
elRango(13)
elRango(21)
vacaciones_o_laburo(17, True)
vacaciones_o_laburo(26, False)
vacaciones_o_laburo(60, True)
vacaciones_o_laburo(60, False)

print('\n--- 6 ---')
uno_a_diez()
pares_10_a_40()
eco_10()
despegue(5)
viaje_tiempo(2024, 2000)
viaje_tiempo_aristoteles(2025)

print('\n--- 7 ---')
s_uno_a_diez()
s_pares_10_a_40()
s_eco_10()
s_despegue(5)
s_viaje_tiempo(2024, 2000)
s_viaje_tiempo_aristoteles(2025)